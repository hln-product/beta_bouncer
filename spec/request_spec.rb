require 'spec_helper'
require_relative '../lib/beta_bouncer/request'

describe BetaBouncer::Request do
  let(:request) { BetaBouncer::Request.new(env) }
  let(:env)     { double(:env) }

  before do
    allow(request).to receive(:beta_access_secret_key).and_return(:valid)
  end

  context "#access_granted?" do
    it "should be true when access key matches cookie access key" do
      allow(request).to receive(:has_valid_access_key?).and_return(true)
      expect(request).to be_access_granted
    end
    it "should be true when path is excluded" do
      allow(request).to receive(:has_valid_access_key?).and_return(false)
      allow(request).to receive(:excluded?).and_return(true)
      expect(request).to be_access_granted
    end
  end

  context "#response" do
    it "should return access_denied_response when params key invalid" do
      allow(request).to receive(:has_valid_params_key?).and_return(false)
      expect(request).to receive(:access_denied_response)
      request.response
    end

    it "should return access_granted_response when params key is valid" do
      allow(request).to receive(:has_valid_params_key?).and_return(true)
      expect(request).to receive(:access_granted_response)
      request.response
    end
  end
end
