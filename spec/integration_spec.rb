require 'spec_helper'
require_relative '../lib/beta_bouncer'

describe BetaBouncer do
  let(:app) { ->(env) { [200, env, "app"] } }
  let(:middleware) do
    BetaBouncer::Server.new(app)
  end

  let(:secret_key) { "abcdefg" }

  def env_for url, opts={}
    Rack::MockRequest.env_for(url, opts)
  end

  before do
    stub_const('ENV', env)
  end

  let(:env) do
    {
      'BETA_ACCESS_SECRET_KEY' => secret_key,
      'BETA_ACCESS_EXCLUDE_PATHS' => "/assets/,/custom/"
    }
  end

  context "without beta access cookie" do
    it "should reject request" do
      code = middleware.call(env_for("http://example.com/")).shift
      expect(code).to eq(401)
    end

    it "shoud allow request if SKIP_BETA_ACCESS_RESTRICTION is present" do
      stub_const('ENV', {'SKIP_BETA_ACCESS_RESTRICTION' => "true"})
      code = middleware.call(env_for("http://example.com/")).shift
      expect(code).to eq(200)
    end

    it "should add beta access cookie when secret param is provided" do
      env = middleware.call(env_for("http://example.com/?beta_access_key=abcdefg"))[1]
      expect(env.fetch("Set-Cookie")).to eq("beta_access_key=abcdefg; path=/")
    end

    it "should redirect with to same location without beta_access_key when secret param is provided" do
      code, env = middleware.call env_for("http://example.com/?beta_access_key=abcdefg")
      expect(code).to eq(301)
      expect(env.fetch("Location")).to eq("http://example.com/")
    end

    it "should redirect with to same location without beta_access_key when secret param is provided,
      existing query string edition" do
      code, env = middleware.call env_for("http://example.com/?a=b&beta_access_key=abcdefg")
      expect(code).to eq(301)
      expect(env.fetch("Location")).to eq("http://example.com/?a=b")
    end

    context "when visiting an excluded path" do
      it "skips beta restriction on excluded paths" do
        code = middleware.call(env_for("http://example.com/assets/foo")).shift
        expect(code).to eq(200)
      end
    end
  end

  context "with beta access cookie" do
    it "should allow requests" do
      code = middleware.call(env_for("http://example.com/","HTTP_COOKIE" => "beta_access_key=abcdefg;")).shift
      expect(code).to eq(200)
    end
  end
end
