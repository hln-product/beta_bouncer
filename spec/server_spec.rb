require 'spec_helper'
require_relative '../lib/beta_bouncer/server'

describe BetaBouncer::Server do
  let(:request) { double(:request) }
  let(:app)     { double(:app)     }
  let(:env)     { double(:env)     }

  context "when override is on" do
    it "delegates to app" do
      server = BetaBouncer::Server.new(app)
      allow(server).to receive(:override_on?).and_return(true)
      expect(app).to receive(:call).with(env)
      server.call(env)
    end
  end

  context "when request access is valid" do
    it "delegate to the app" do
      allow(request).to receive(:access_granted?).and_return(true)
      server = BetaBouncer::Server.new(app)
      allow(server).to receive(:generate_beta_bouncer_request).with(env).and_return(request)
      expect(app).to receive(:call).with(env)
      server.call(env)
    end
  end

  context "when request access is not valid" do
    it "delegates to request" do
      allow(request).to receive(:access_granted?).and_return(false)
      expect(request).to receive(:response)
      server = BetaBouncer::Server.new(app)
      allow(server).to receive(:generate_beta_bouncer_request).with(env).and_return(request)
      server.call(env)
    end
  end
end
