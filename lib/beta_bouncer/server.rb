class BetaBouncer::Server
  def initialize(app)
    @app = app
  end

  def call(env)
    return(@app.call(env)) if override_on?

    request = generate_beta_bouncer_request(env)
    if request.access_granted?
      @app.call(env)
    else
      request.response
    end
  end

  private
  def generate_beta_bouncer_request(env)
    BetaBouncer::Request.new(env)
  end

  def override_on?
    ENV.fetch("SKIP_BETA_ACCESS_RESTRICTION") { false }
  end
end
