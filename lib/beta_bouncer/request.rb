require 'forwardable'
require 'rack'
require 'csv'

class BetaBouncer::Request
  extend Forwardable

  def initialize(env)
    @rack_request = Rack::Request.new(env)
  end

  def access_granted?
    has_valid_access_key? || excluded?
  end

  def response
    if has_valid_params_key?
      access_granted_response
    else
      access_denied_response
    end
  end

  private

  def_delegators :@rack_request, :cookies, :params, :base_url, :path

  def access_granted_response
    headers = {
      "Content-Type" => "text",
      "Location" => redirect_url,
      "Set-Cookie"=>"beta_access_key=#{beta_access_secret_key}; path=/"
    }
    [301, headers, ["Redirecting to #{redirect_url} ..."]]
  end

  def access_denied_response
    [401, {}, [""]]
  end

  def redirect_url
    new_params = params.reject {|k,v| k == "beta_access_key" }
    new_query_string = Rack::Utils.build_query(new_params)
    new_query_string = "?#{new_query_string}" unless new_query_string.empty?
    "#{base_url}#{path}#{new_query_string}"
  end

  def beta_access_secret_key
    ENV.fetch("BETA_ACCESS_SECRET_KEY")
  end

  def has_valid_params_key?
    params["beta_access_key"] == beta_access_secret_key
  end

  def has_valid_access_key?
    cookies["beta_access_key"] == beta_access_secret_key
  end

  def excluded?
    exclude_paths.detect {|exclude_path| path.index(exclude_path) == 0 }
  end

  def exclude_paths
    self.class.exclude_paths
  end

  def self.exclude_paths
    @exclude_paths ||= load_exlude_paths
  end

  def self.load_exlude_paths
    CSV.parse(ENV.fetch("BETA_ACCESS_EXCLUDE_PATHS")).flatten
  rescue KeyError
    Array.new
  end
end
