class BetaBouncer::Railtie < Rails::Railtie
  initializer "beta_bouncer.insert_middleware" do |app|
    app.config.middleware.use "BetaBouncer::Server"
  end
end
