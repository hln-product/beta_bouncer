# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'beta_bouncer/version'

Gem::Specification.new do |spec|
  spec.name          = "beta_bouncer"
  spec.version       = BetaBouncer::VERSION
  spec.authors       = ["Sam Schenkman-Moore"]
  spec.email         = ["samuel.schenkman-moore@turner.com"]
  spec.summary       = %q{Keeps people without a secret key out of the site.}
  spec.description   = %q{Keeps people without a secret key out of the site.
                          After they pass that key in through a query string,
                          a cookie keeps them permitted.}
  spec.homepage      = ""
  # spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.6"
  spec.add_development_dependency "rake"
  spec.add_development_dependency "rack"
  spec.add_development_dependency "rspec"
end
