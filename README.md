# BetaBouncer

[ ![Codeship Status for hln-product/beta_bouncer](https://www.codeship.io/projects/956be940-fcc4-0131-1c00-62f9faf58206/status)](https://www.codeship.io/projects/29385)

Keeps uninvited public away from an app that isn't ready.

Craft links like this: http://mysite.example.com/?beta_access_key=abc123
... for people you want to see the site.

## Installation

Add to gemfile:

`gem "beta_bouncer", git: 'https://bitbucket.org/hln-product/beta_bouncer.git'`

Add environment variable `BETA_ACCESS_SECRET_KEY` with some hard-to-guess value.

Add environment variable `SKIP_BETA_ACCESS_RESTRICTION` with any truthy value to override.

Add environment variable `BETA_ACCESS_EXCLUDE_PATHS` with CSV separated path patterns that should not be beta access restricted.

In your feature/integration specs you may need to:

```
RSpec.configure do |config|
  config.before(:all) do
    ENV["SKIP_BETA_ACCESS_RESTRICTION"] = "skip beta restriction in test"
  end
end

```
... or such
