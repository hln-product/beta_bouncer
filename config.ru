require 'rubygems'
require 'bundler/setup'
require "./lib/beta_bouncer"

use BetaBouncer::Server
app = proc do |env|
  [ 200, {'Content-Type' => 'text/plain'}, ["Access Granted"] ]
end
run app